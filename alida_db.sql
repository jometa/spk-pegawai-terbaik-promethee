-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 21, 2019 at 05:33 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alida_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `alida_db_karyawan`
--

CREATE TABLE `alida_db_karyawan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggalLahir` datetime NOT NULL,
  `tempatLahir` varchar(255) NOT NULL,
  `jenisKelamin` enum('LAKI_LAKI','PEREMPUAN') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alida_db_karyawan`
--

INSERT INTO `alida_db_karyawan` (`id`, `nama`, `tanggalLahir`, `tempatLahir`, `jenisKelamin`) VALUES
(1, 'Simon Petrus Pallo', '1990-09-19 08:00:00', 'Kupang', 'LAKI_LAKI'),
(2, 'Yohance Nahor Kikhau', '1990-09-19 08:00:00', 'Kupang', 'LAKI_LAKI'),
(3, 'Kayetenus Piteronoris Pati Piel', '1990-09-19 08:00:00', 'Kupang', 'LAKI_LAKI'),
(4, 'Yeremias Ninu', '1990-09-19 08:00:00', 'Kupang', 'LAKI_LAKI'),
(5, 'Asian Parningotan Darmanik', '1990-09-19 08:00:00', 'Kupang', 'LAKI_LAKI');

-- --------------------------------------------------------

--
-- Table structure for table `alida_db_kriteria`
--

CREATE TABLE `alida_db_kriteria` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alida_db_kriteria`
--

INSERT INTO `alida_db_kriteria` (`id`, `nama`) VALUES
(1, 'Disiplin'),
(2, 'Absensi'),
(3, 'Penampilan');

-- --------------------------------------------------------

--
-- Table structure for table `alida_db_penilaian`
--

CREATE TABLE `alida_db_penilaian` (
  `id` int(11) NOT NULL,
  `subkriteria_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alida_db_penilaian`
--

INSERT INTO `alida_db_penilaian` (`id`, `subkriteria_id`, `karyawan_id`, `nilai`) VALUES
(13, 1, 1, 0),
(14, 6, 1, 0),
(15, 9, 1, 0),
(16, 3, 2, 0),
(17, 6, 2, 0),
(18, 10, 2, 0),
(19, 3, 3, 0),
(20, 7, 3, 0),
(21, 9, 3, 0),
(22, 2, 4, 0),
(23, 6, 4, 0),
(24, 9, 4, 0),
(25, 1, 5, 0),
(26, 5, 5, 0),
(27, 12, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `alida_db_sub_kriteria`
--

CREATE TABLE `alida_db_sub_kriteria` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `bobot` double NOT NULL,
  `kriteria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alida_db_sub_kriteria`
--

INSERT INTO `alida_db_sub_kriteria` (`id`, `nama`, `bobot`, `kriteria_id`) VALUES
(1, 'A', 1, 1),
(2, 'B', 2, 1),
(3, 'C', 3, 1),
(4, 'D', 4, 1),
(5, 'A', 1, 2),
(6, 'B', 2, 2),
(7, 'C', 3, 2),
(8, 'D', 4, 2),
(9, 'A', 1, 3),
(10, 'B', 2, 3),
(11, 'C', 3, 3),
(12, 'D', 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `alida_db_user`
--

CREATE TABLE `alida_db_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alida_db_user`
--

INSERT INTO `alida_db_user` (`id`, `nama`, `username`, `password`) VALUES
(1, 'Alida Siahaan', 'alida-user', 'alida-user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alida_db_karyawan`
--
ALTER TABLE `alida_db_karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alida_db_kriteria`
--
ALTER TABLE `alida_db_kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alida_db_penilaian`
--
ALTER TABLE `alida_db_penilaian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_a222c96cf1af07f8e8f409d43ec` (`subkriteria_id`),
  ADD KEY `FK_e6f622d9e11dbd290b611f481f2` (`karyawan_id`);

--
-- Indexes for table `alida_db_sub_kriteria`
--
ALTER TABLE `alida_db_sub_kriteria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ffd86ce1299f925be82db8e6f3c` (`kriteria_id`);

--
-- Indexes for table `alida_db_user`
--
ALTER TABLE `alida_db_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alida_db_karyawan`
--
ALTER TABLE `alida_db_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `alida_db_kriteria`
--
ALTER TABLE `alida_db_kriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `alida_db_penilaian`
--
ALTER TABLE `alida_db_penilaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `alida_db_sub_kriteria`
--
ALTER TABLE `alida_db_sub_kriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `alida_db_user`
--
ALTER TABLE `alida_db_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `alida_db_penilaian`
--
ALTER TABLE `alida_db_penilaian`
  ADD CONSTRAINT `FK_a222c96cf1af07f8e8f409d43ec` FOREIGN KEY (`subkriteria_id`) REFERENCES `alida_db_sub_kriteria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_e6f622d9e11dbd290b611f481f2` FOREIGN KEY (`karyawan_id`) REFERENCES `alida_db_karyawan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `alida_db_sub_kriteria`
--
ALTER TABLE `alida_db_sub_kriteria`
  ADD CONSTRAINT `FK_ffd86ce1299f925be82db8e6f3c` FOREIGN KEY (`kriteria_id`) REFERENCES `alida_db_kriteria` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
