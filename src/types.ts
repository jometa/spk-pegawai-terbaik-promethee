import { Repository } from 'typeorm';
import { Karyawan } from '@models/Karyawan';
import { Kriteria } from '@models/Kriteria';
import { SubKriteria } from '@models/SubKriteria';
import { Penilaian } from '@models/Penilaian';
import { User } from '@models/User';

type DbRepo = {
  karyawan: Repository<Karyawan>;
  kriteria: Repository<Kriteria>;
  subkriteria: Repository<SubKriteria>;
  penilaian: Repository<Penilaian>;
  user: Repository<User>;
};

declare global {
  namespace Express {
    export interface Request {
      db_repo: DbRepo;
      user?: User;
    }
  }
};

