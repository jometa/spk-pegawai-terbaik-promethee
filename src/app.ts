import "reflect-metadata";
import express, { Application }  from 'express';
import cookieSession from 'cookie-session';
import responseTime from 'response-time';
import serveStatic from 'serve-static';
import { createConnection } from 'typeorm';
import moment from 'moment';
import morgan from 'morgan';
import morganChalk from './morgan-chalk';
import { join } from 'path';
import { cwd } from 'process';

import { Karyawan, JenisKelamin } from '@models/Karyawan';
import { Kriteria } from '@models/Kriteria';
import { SubKriteria } from '@models/SubKriteria';
import { Penilaian } from '@models/Penilaian';
import { User } from '@models/User';
import { routes } from '@routes/index';

export async function boot(): Promise<Application> {
  const app: Application = express();
  
  app.use(cookieSession({
    name: 'alida-promethee',
    secure: false,
    keys: ['foobar']
  }));
  app.use(responseTime());
  app.use(morganChalk);

  app.set('view engine', 'pug');
  app.locals.basedir = join(cwd(), 'views');
  app.locals.moment = moment;
  app.locals.formatSex = (jk: JenisKelamin) => {
    if (jk == JenisKelamin.LAKI_LAKI) {
      return 'Laki - Laki';
    } else {
      return 'Perempuan';
    }
  };

  const connection = await createConnection({
    type: 'mariadb',
    host: '127.0.0.1',
    database: 'alida_db',
    entityPrefix: 'alida_db_',
    port: 3306,
    username: 'root',
    password: '',
    synchronize: false,
    logging: true,
    entities: [
      Karyawan,
      Kriteria,
      SubKriteria,
      Penilaian,
      User
    ]
  });

  app.use(async (req, resp, next) => {
    req.db_repo = {
      karyawan: connection.getRepository<Karyawan>(Karyawan),
      kriteria: connection.getRepository<Kriteria>(Kriteria),
      subkriteria: connection.getRepository<SubKriteria>(SubKriteria),
      penilaian: connection.getRepository<Penilaian>(Penilaian),
      user: connection.getRepository<User>(User)
    };
    next();
  });

  routes(app);

  app.use('/resources', serveStatic('static'));

  app.use((err, req, resp, next) => {
    if (err && err.error && err.error.isJoi) {
      resp.status(400).json({
        errors: err.error.details.map(d => d.context.key)
      })
    } else {
      next(err);
    }
  });

  return app;
}
