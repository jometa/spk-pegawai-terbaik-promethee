import Combinatorics from 'js-combinatorics';
import { zip, sum } from 'lodash';

export type Row = {
  id: any;
  data: number[];
}

export type RowPref = {
  a: any;
  b: any;
  deltas?: number[];
  pi?: number;
}

export type RowResult = {
  net: number;
  enter: number;
  leave: number;
};

const H = (d: number) => d <= 0 ? 0 : 1;

export function promethee(rows: Row[], weights: number[]) {
  const all_id = rows.map(r => r.id);
  const n = rows.length;
  const nkrit = weights.length;
  const perm = Combinatorics.permutation(rows, 2);

  let prefs: RowPref[] = perm.toArray().map(pair => {
    const a = pair[0].id;
    const b = pair[1].id;
    const a_data = pair[0].data;
    const b_data = pair[1].data;
    let diffs = [];
    for (let ikrit = 0; ikrit < nkrit; ikrit++) {
      const diff = a_data[ikrit] - b_data[ikrit];
      const d = H(diff);
      diffs.push(d);
    }
    const row_pref: RowPref = {
      a,
      b,
      deltas: diffs
    };

    return row_pref;
  });

  prefs.map((rp: RowPref) => {
    rp.pi = (1.0 / nkrit) * sum( zip(rp.deltas, weights).map(pair => pair[0] * pair[1]) );
    return rp;
  });

  const nets: RowResult[] = all_id.map(id => {
    const leave: number = (1.0 / (n - 1)) * sum( prefs.filter(rp => rp.a == id).map(rp => rp.pi) );
    const enter: number = (1.0 / (n - 1)) * sum( prefs.filter(rp => rp.b == id).map(rp => rp.pi) );
    const net = leave - enter;

    return {
      net,
      leave,
      enter
    };
  });

  return nets;
}