import { boot } from './app';

(async () => {
  const app = await boot();
  app.listen(5000, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`Listening at http://127.0.0.1:5000`);
    }
  });
})();