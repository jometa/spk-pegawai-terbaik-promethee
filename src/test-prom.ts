import { Row, promethee } from '@services/promethee';

const data: Row[] = [
  {
    id: 'A',
    data: [4, 2, 3]
  },
  {
    id: 'B',
    data: [1, 1, 2]
  },
  {
    id: 'C',
    data: [2, 3, 4]
  },
  {
    id: 'D',
    data: [3, 2, 2],
  },
  {
    id: 'E',
    data: [1, 4, 3]
  }
];

console.log('here');
const result = promethee(data, [1, 1, 1]);
console.log(result);
console.log('and here');
