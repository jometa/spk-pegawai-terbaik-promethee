import { Application } from 'express';
import multer from 'multer';
import { Penilaian } from '@models/Penilaian';

export default async function (app: Application, basepath: string) {

  app.get(`${basepath}/list`, async (req, resp) => {
    let items = await req.db_repo.karyawan.find({
      relations: [
        'dataPenilaian',
        'dataPenilaian.subkriteria',
        'dataPenilaian.subkriteria.kriteria'
      ]
    });

    const kriteria_items = await req.db_repo.kriteria.find();

    items = items.map(it => {
      let valid = true;
      if (it.dataPenilaian.length != kriteria_items.length) {
        valid = false;
      }

      const nilais = kriteria_items.map(krit => {
        const kid = krit.id;
        const nilai = it.dataPenilaian.find(nilai => {
          return nilai.subkriteria.kriteria_id == kid;
        });
        if (!nilai) {
          return {
            nama: '-',
            id: undefined
          }
        } else {
          return nilai.subkriteria;
        }
      });

      return {
        ...it,
        valid,
        nilais
      };
    })

    resp.render('app/penilaian/list', {
      items,
      kriteria_items
    });
  });

  app.get(`${basepath}/edit/:id`, async (req, resp) => {
    const id = parseInt(req.params.id);
    let item = await req.db_repo.karyawan.findOne(id, {
      relations: [
        'dataPenilaian',
        'dataPenilaian.subkriteria',
        'dataPenilaian.subkriteria.kriteria'
      ]
    });

    const kriteria_items = await req.db_repo.kriteria.find({
      relations: ['subs']
    });
  
    const nilais = kriteria_items.map(krit => {
      const kid = krit.id;

      const nilai = item.dataPenilaian.find(nilai => {
        return nilai.subkriteria.kriteria_id == kid;
      });

      const subs = krit.subs.map(sub => ({
        text: `${sub.nama} - ${sub.bobot}`,
        value: sub.id
      }));

      let _nilai: any;
      if (!nilai) {
        _nilai = '-';
      } else {
        _nilai = nilai.subkriteria_id;
      }
      return {
        nilai: _nilai,
        kriteria: {
          ...krit,
          subs
        }
      }
    });

    console.log(nilais);

    const payload = {
      item,
      nilais
    };

    resp.render(`app/penilaian/edit`, payload);
  });

  app.post(`${basepath}/edit/:id`, multer().none(), async (req, resp) => {
    const id = parseInt(req.params.id);

    const items: any = req.body;

    const nilais = Object.keys(items).map(key => {
      const subkriteria_id = parseInt(items[key]);
      return {
        subkriteria_id,
        karyawan_id: id
      } as Penilaian;
    });

    await req.db_repo.penilaian.createQueryBuilder()
      .delete()
      .where("karyawan_id = :id", { id })
      .execute();

    await req.db_repo.penilaian.createQueryBuilder()
      .insert()
      .values(nilais)
      .execute();
    
    resp.redirect(`/app/penilaian/edit/${id}`);
  });
}
