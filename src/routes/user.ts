import { Application } from 'express';
import multer from 'multer';
import { User, schema } from '@models/User';

export default function (app: Application, basepath: string) {

  app.get(`${basepath}/list`, async (req, resp) => {
    const items = await req.db_repo.user.find();
    resp.render('app/user/list', {
      items
    });
  });

  app.get(`${basepath}/edit/:id`, async (req, resp) => {
    const item = await req.db_repo.user.findOne(req.params.id);
    resp.render('app/user/edit/:id', {
      item
    });
  });

  app.post(`${basepath}/edit/:id`, multer().none(),  async (req, resp) => {
    const id = parseInt(req.params.id);
    const payload: Partial<User> = req.body;
    let item = await req.db_repo.user.findOneOrFail(id);
    item = { ...item, ...payload };
    await req.db_repo.user.save(item);
    resp.redirect('app/user/list');
  });

  app.get(`${basepath}/delete/:id`, async (req, resp) => {
    const id = parseInt(req.params.id);
    await req.db_repo.user.delete(id);
    resp.redirect('/app/user/list');
  });

  app.get(`${basepath}/tambah`, (req, resp) => {
    resp.render('app/user/tambah');
  });

  app.post(`${basepath}/tambah`, multer().none(), async (req, resp) => {
    const payload: Partial<User> = req.body;
    let user = req.db_repo.user.create(payload);
    user = await req.db_repo.user.save(user);
    resp.redirect('/app/user/list');
  });

}
