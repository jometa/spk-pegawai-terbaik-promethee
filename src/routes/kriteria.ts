import { Application } from 'express';
import multer from 'multer';
import { Kriteria, schema } from '@models/Kriteria';
import { SubKriteria } from '@models/SubKriteria';

export default function (app: Application, basepath: string) {

  app.get(
    `${basepath}/list`,
    async (req, resp) => {
      const repo = req.db_repo.kriteria;
      const items = await repo.find({
        relations: ['subs']
      });
      resp.render('app/kriteria/list', {
        items
      });
    }
  );

  app.get(
    `${basepath}/tambah`, 
    (req, resp) => {
      resp.render('app/kriteria/tambah');
    });

  app.post(
    `${basepath}/tambah`,
    multer().none(),
    async (req, resp) => {
      try {
        await schema.validate(req.body, {
          abortEarly: false
        });
        const payload: Partial<Kriteria> = req.body;
        const repo = req.db_repo.kriteria;
        let kriteria = repo.create(payload);
        kriteria = await repo.save(kriteria);
        resp.redirect('/app/kriteria/list');
      } catch (err) {
        const errors = err.details.map(d => ({
          key: d.context.key,
          message: d.message
        }));
        resp.render('app/kriteria/tambah', {
          errors
        });
      }
    });

  app.get(
    `${basepath}/edit/:id`, 
    async (req, resp) => {
      const repo = req.db_repo.kriteria;
      const id = req.params.id;
      const item = await repo.findOneOrFail(id);
      resp.render('app/kriteria/edit', {
        item
      });
    });

  app.post(
    `${basepath}/edit/:id`,
    multer().none(),
    async (req, resp) => {
      try {
        await schema.validate(req.body, {
          abortEarly: false
        });
        
        const payload: Partial<Kriteria> = req.body;
        const id = req.params.id;
        const repo = req.db_repo.kriteria;
        try {
          let item = await repo.findOneOrFail(id)
          item = { ...item, ...payload };
          await repo.save(item);
          resp.redirect('/app/kriteria/list');
        } catch (err) {
          console.log(err);
          resp.status(500).send('gagal');
        }
      } catch (err) {
        const errors = err.details.map(d => ({
          key: d.context.key,
          message: d.message
        }));
        resp.render('app/kriteria/tambah', {
          errors
        });
      }
    }
  );

  app.get(
    `${basepath}/delete/:id`,
    async (req, resp) => {
      const id = req.params.id;
      const repo = req.db_repo.kriteria;
      await repo.delete(id);
      resp.redirect('/app/kriteria/list');
    }
  );

  app.get(`${basepath}/detail/:id/subs`, async (req, resp) => {
    const id = req.params.id;
    const repo = req.db_repo.kriteria;
    const item = await repo.findOneOrFail(id, {
      relations: [
        'subs'
      ]
    });

    resp.render('app/kriteria/detail', {
      item
    });
  });

  app.get(`${basepath}/detail/:id/subs/tambah`, async (req, resp) => {
    const id = req.params.id;
    const repo = req.db_repo.kriteria;
    const item = await repo.findOneOrFail(id);
    resp.render('app/kriteria/sub-tambah', {
      item
    });
  });

  app.post(`${basepath}/detail/:id/subs/tambah`, multer().none(), async (req, resp) => {
    const id = parseInt(req.params.id);
    const payload: Partial<SubKriteria> = req.body;
    const repo = req.db_repo.subkriteria;
    let item = { ...payload, kriteria_id: id };
    await repo.save(item);
    resp.redirect(`/app/kriteria/detail/${id}/subs`);
  });

  app.get(`${basepath}/detail/:id/subs/edit/:subid`, async (req, resp) => {
    const krit_repo = req.db_repo.kriteria;
    const sub_repo = req.db_repo.subkriteria;
    const krit_id = parseInt(req.params.id);
    const sub_id = parseInt(req.params.subid);

    const item = await sub_repo.findOne(sub_id, {
      relations: ['kriteria']
    });

    resp.render('app/kriteria/sub-edit', {
      item
    });
  });

  app.post(`${basepath}/detail/:id/subs/edit/:subid`, multer().none(), async (req, resp) => {
    const sub_id = parseInt(req.params.subid);
    const sub_repo = req.db_repo.subkriteria;
    const payload: Partial<SubKriteria> = req.body;
    let item = await sub_repo.findOne(sub_id);

    item = { ...item, ...payload };
    await sub_repo.save(item);
    resp.redirect(`${basepath}/detail/${req.params.id}/subs`);
  });
}