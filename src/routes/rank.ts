import { Application } from 'express';
import multer from 'multer';
import { Row, promethee, RowResult } from '@services/promethee';
import { sortBy, zip } from 'lodash';
import { Karyawan } from '@models/Karyawan';

type KaryawanRanked = {
  karyawan: Karyawan;
  result: RowResult;
}

export default function (app: Application, basepath: string) {
  
  // app.get(`${basepath}`, async (req, resp) => {
  //   const kriteria_items = await req.db_repo.kriteria.find({
  //     order: {
  //       id: 'ASC'
  //     }
  //   });
  //   resp.render('app/rank/form', {
  //     kriteria_items
  //   });
  // });

  app.get(`${basepath}`, async (req, resp) => {
    const _weights = [1, 1, 1];
    const ordered_keys = sortBy(Object.keys(_weights));
    const weights: number[] = ordered_keys.map(k => _weights[k] as number);

    const kriteria_items = await req.db_repo.kriteria.find({
      order: {
        id: 'ASC'
      }
    });

    const karyawan_items = await req.db_repo.karyawan.find({
      relations: [
        'dataPenilaian',
        'dataPenilaian.subkriteria',
        'dataPenilaian.subkriteria.kriteria'
      ]
    });

    const rows: Row[] = karyawan_items.map(({ id, dataPenilaian }) => {

      const xs: number[] = kriteria_items.map(krit => {
        let result = dataPenilaian.find(penilaian => penilaian.subkriteria.kriteria_id == krit.id);
        if (!result) {
          throw new Error(`Unknown penilaian for kriteria=${krit} and karyawann=${id}`);
        }
        return result.subkriteria.bobot;
      });

      return {
        id,
        data: xs
      };

    });

    const nets = promethee(rows, weights);
    let ranked: KaryawanRanked[] = zip(karyawan_items, nets).map(([ karyawan, result ]) => ({
      karyawan,
      result
    }));

    ranked = sortBy(ranked, item => item.result.net);

    resp.render('app/rank/rank', {
      items: ranked
    });
  });

  app.post(`${basepath}`, multer().none(), async (req, resp) => {
    const _weights = req.body;
    const ordered_keys = sortBy(Object.keys(_weights));
    const weights: number[] = ordered_keys.map(k => _weights[k] as number);

    const kriteria_items = await req.db_repo.kriteria.find({
      order: {
        id: 'ASC'
      }
    });

    const karyawan_items = await req.db_repo.karyawan.find({
      relations: [
        'dataPenilaian',
        'dataPenilaian.subkriteria',
        'dataPenilaian.subkriteria.kriteria'
      ]
    });

    const rows: Row[] = karyawan_items.map(({ id, dataPenilaian }) => {

      const xs: number[] = kriteria_items.map(krit => {
        let result = dataPenilaian.find(penilaian => penilaian.subkriteria.kriteria_id == krit.id);
        if (!result) {
          throw new Error(`Unknown penilaian for kriteria=${krit} and karyawann=${id}`);
        }
        return result.subkriteria.bobot;
      });

      return {
        id,
        data: xs
      };

    });

    const nets = promethee(rows, weights);
    let ranked: KaryawanRanked[] = zip(karyawan_items, nets).map(([ karyawan, result ]) => ({
      karyawan,
      result
    }));

    ranked = sortBy(ranked, item => item.result.net);

    resp.render('app/rank/rank', {
      items: ranked
    });
  });

}