import { Application } from 'express';
import multer from 'multer';
import { SubKriteria, schema } from '@models/SubKriteria';

export default function (app: Application, basepath: string) {

  app.get(`${basepath}/tambah`, async (req, resp) => {
    const krit_repo = req.db_repo.kriteria
    const kriteria_items = await krit_repo.find();
    resp.render('app/subkriteria/tambah', {
      kriteria_items
    });
  });

  app.post(`${basepath}/tambah`, multer().none(), async (req, resp) => {
    const payload: Partial<SubKriteria> = req.body;
    const repo = req.db_repo.subkriteria;
    let sub = repo.create(payload);
    await repo.save(sub);
    resp.redirect('/app/kriteria/list');
  });

  app.get(`${basepath}/edit/:id`);
  app.post(`${basepath}/edit/:id`);
  app.get(`${basepath}/delete/:id`);

}
