import { Application } from 'express';
import multer from 'multer';
import { Karyawan } from '@models/Karyawan';

export default function (app: Application, basepath: string) {

  app.get(`${basepath}/list`, async (req, resp) => {
    const repo = req.db_repo.karyawan;
    const items = await repo.find();
    resp.render('app/karyawan/list', {
      items
    });
  });

  app.get(`${basepath}/tambah`, (req, resp) => {
    resp.render('app/karyawan/tambah');
  });

  app.post(`${basepath}/tambah`, multer().none(), async (req, resp) => {
    const repo = req.db_repo.karyawan;
    let payload: Partial<Karyawan> = req.body;
    let karyawan: Karyawan = repo.create(payload);
    await repo.save(karyawan);
    resp.redirect(`${basepath}/list`);
  });

  app.get(`${basepath}/edit/:id`, async (req, resp) => {
    const repo = req.db_repo.karyawan;
    const item = await repo.findOne(req.params.id);
    resp.render('app/karyawan/edit', {
      item
    });
  });

  app.post(`${basepath}/edit/:id`, multer().none(), async (req, resp) => {
    const id = req.params.id;
    const payload: Partial<Karyawan> = req.body;
    const repo = req.db_repo.karyawan;
    let item = await repo.findOneOrFail(id);
    item = { ...item, ...payload };
    await repo.save(item);
    resp.redirect('/app/karyawan/list');
  });

  app.get(`${basepath}/delete/:id`, async (req, resp) => {
    await req.db_repo.karyawan.delete(req.params.id);
    resp.redirect('/app/karyawan/list');
  });

};