import { Application } from 'express';
import multer from 'multer';
import karyawan_routes from '@routes/karyawan';
import kriteria_routes from '@routes/kriteria';
import subkriteria_routes from '@routes/subkriteria';
import user_routes from '@routes/user';
import penilaian_routes from '@routes/penilaian';
import rank_routes from '@routes/rank';

export async function routes (app: Application) {
  app.get('/', async (req, resp) => {
    resp.render('landing/index');
  });

  // Check login
  app.use('/app', (req, resp, next) => {
    const username = req.session.username;
    if (!username) {
      resp.redirect('/login');
    } else {
      next();
    }
  });

  app.get('/login', (req, resp) => {
    const username = req.session.username;
    if (!username) {
      resp.render('landing/login');
    } else {
      resp.redirect('/app');
    }
  });

  app.get('/logout', (req, resp) => {
    req.session = null;
    resp.redirect('/');
  });

  app.post('/login', multer().none(), async (req, resp) => {
    const { username, password } = req.body;
    console.log(req.body);
    const user = await req.db_repo.user.findOne({
      username
    });
    if (!user) {
      resp.redirect('/login');
      return;
    }
    if (user.password != password) {
      resp.redirect('/login');
      return;
    }
    req.session.username = username;
    req.session.user_nama = user.nama;
    resp.redirect('/app/karyawan/list');
  });

  app.get('/app', async (req, resp) => {
    resp.redirect('/app/dashboard');
  });

  app.get('/app/dashboard', async (req, resp) => {
    resp.render('app/index');
  });

  karyawan_routes(app, "/app/karyawan");
  kriteria_routes(app, '/app/kriteria');
  subkriteria_routes(app, '/app/subkriteria');
  user_routes(app, '/app/user');
  penilaian_routes(app, '/app/penilaian');
  rank_routes(app, '/app/rank');
}