import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { SubKriteria } from './SubKriteria';
import { Karyawan } from './Karyawan';

@Entity()
export class Penilaian {
  @PrimaryGeneratedColumn()
  id: number;
  
  @ManyToOne(type => SubKriteria)
  @JoinColumn({
    name: 'subkriteria_id'
  })
  subkriteria: SubKriteria;

  @Column()
  subkriteria_id: number;

  @ManyToOne(type => Karyawan)
  @JoinColumn({
    name: 'karyawan_id'
  })
  karyawan: Karyawan;

  @Column()
  karyawan_id: number;

}