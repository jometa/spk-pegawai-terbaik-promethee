import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Penilaian } from './Penilaian';

export enum JenisKelamin {
  LAKI_LAKI='LAKI_LAKI',
  PEREMPUAN='PEREMPUAN'
}

@Entity()
export class Karyawan {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nama: string;

  @Column()
  tanggalLahir: Date;

  @Column()
  tempatLahir: string;

  @Column({
    type: 'enum',
    enum: JenisKelamin
  })
  jenisKelamin: JenisKelamin;

  @OneToMany(type => Penilaian, penilaian => penilaian.karyawan)
  dataPenilaian: Penilaian[];
}
