import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Kriteria } from './Kriteria';
import Joi from '@hapi/joi';

@Entity()
export class SubKriteria {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nama: string;

  @Column('double')
  bobot: number;

  @ManyToOne(type => Kriteria, {
    onDelete: 'CASCADE'
  })
  @JoinColumn({
    name: 'kriteria_id'
  })
  kriteria: Kriteria;

  @Column()
  kriteria_id: number;
}

export const schema = Joi.object({
  nama: Joi.string().required().not(Joi.string().empty()),
  bobot: Joi.number().required().greater(0),
  kriteria_id: Joi.number().required().integer().greater(0)
});
