import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { SubKriteria } from './SubKriteria';
import Joi from '@hapi/joi';

@Entity()
export class Kriteria {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nama: string;

  @OneToMany(type => SubKriteria, sub => sub.kriteria)
  subs: SubKriteria[];
}

export const schema = Joi.object({
  nama: Joi.string().required().not(Joi.string().empty())
});

