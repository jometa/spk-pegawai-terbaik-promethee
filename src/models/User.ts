import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import Joi from '@hapi/joi';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nama: string;

  @Column()
  username: string;

  @Column()
  password: string;
}

export const schema = Joi.object({
  nama: Joi.string().required().not(Joi.string().empty()),
  username: Joi.string().required().not(Joi.string().empty()).min(8),
  password: Joi.string().required().not(Joi.string().empty())
});
